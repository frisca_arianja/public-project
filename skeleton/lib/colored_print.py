txt_colors_list = {
    "black": "30",
    "red": "31",
    "green": "32",
    "yellow": "33",
    "blue": "34",
    "purple": "35",
    "cyan": "36",
    "white": "37"
}

bg_colors_list = {
    "black": "40",
    "red": "41",
    "green": "42",
    "yellow": "43",
    "blue": "44",
    "purple": "45",
    "cyan": "46",
    "white": "47"
}


def stop_coloring() -> None:
    print("\033[0m", end='')


def colored_print(text: str,
                  txt_color: str = "-",
                  bg_color: str = "-",
                  stop: bool = True) -> None:
    if bg_color == "-":
        print("\033[0;" + txt_colors_list[txt_color] + "m" + text, end='')
    elif txt_color == "-":
        print("\033[0;0;" + bg_colors_list[bg_color] + "m" + text, end='')
    elif txt_color == "-" and bg_color == "-":
        print(text, end='')
    else:
        print("\033[0;" + txt_colors_list[txt_color] + ";" +
              bg_colors_list[bg_color] + "m" + text,
              end='')

    if stop:
        stop_coloring()
