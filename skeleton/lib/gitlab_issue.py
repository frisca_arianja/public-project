from typing import Union
from os import makedirs, stat
from os.path import normpath
from json import loads, dumps

from .lib.exception.exception import exc_to_generator

M = Union[Exception, str]

class GitlabIssue:
    def __init__(self, case_id: int, folder_name: str = "./issues"):
        self.__folder_name = folder_name
        self.__case_id = case_id
        self.__file_name = "{}.json".format(case_id)

        try:
            makedirs(self.__folder_name, exist_ok=True)
        except Exception as exc:
            raise Exception("create class GitlabIssue error: {}".format(str(exc)))

    @property
    def folder_name(self) -> str:
        return self.__folder_name
    
    @property
    def case_id(self) -> int:
        return self.__case_id
    
    @property
    def file_name(self) -> str:
        return self.__file_name
    
    def create_issue(self, err_message: M, browser: str = None):
        title_error = "Error"
        if type(err_message) is Exception:
            title_error = err_message.__class__.__name__
        
        title_issue = "C{} - {}".format(self.case_id, title_error)
        if browser:
            title_issue =  "{} - {}".format(title_issue, browser)

        issue = {"title": title_issue, "assignee_ids": [], "milestone_id": "", "labels": ""}

        description = ""

        if type(err_message) is str:
            description = err_message
        else:
            for line in exc_to_generator(err_message):
                description += line
        
        issue["description"] = description

        issue_data = {}

        f = open(
            normpath("{}/C{}".format(self.folder_name, self.file_name)), mode="a+")

        if not stat(normpath("{}/C{}".format(self.folder_name, self.file_name))).st_size:
            issue_data = {"issues": [issue]}
        else:
            f.seek(0)
            issue_data = loads(f.readline())

            issues = issue_data.get("issues", [])

            issues.append(issue)

        f = open(
            normpath("{}/C{}".format(self.folder_name, self.file_name)), mode="w")

        f.write("{}\n".format(dumps(issue_data)))

        f.close()



    