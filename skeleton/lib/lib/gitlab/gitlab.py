from typing import Dict, Any

from .options import Options
from .issues import Issues


class Gitlab:
    def __init__(self, domain: str = 'https://www.gitlab.com', username: str = None, password: str = None, token: str = None):
        self.__options = Options(domain, username, password, token)

    @property
    def options(self) -> Options:
        return self.__options

    def issues(self, params: Dict[str, Any] = {}) -> Issues:
        return Issues(self.options, params)
