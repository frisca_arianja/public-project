from traceback import TracebackException
from typing import List, Generator


def exc_to_list(exc: BaseException) -> List[str]:
    list_str_exc = []

    for line in TracebackException(type(exc), exc, exc.__traceback__).format():
        list_str_exc.append(line)

    return list_str_exc


def exc_to_generator(exc: BaseException) -> Generator[str, None, None]:
    for line in TracebackException(type(exc), exc, exc.__traceback__).format():
        yield line
