from os import makedirs
from os.path import normpath
from datetime import datetime
from typing import Iterable, Union


class Logger:
    ERROR = "ERROR"
    WARNING = "WARNING"
    SUCCESS = "SUCCESS"

    def __init__(self, folder_name: str = "./logging", file_name: str = "main.log"):
        self.__folder_name = folder_name
        self.__file_name = file_name

        try:
            makedirs(self.__folder_name, exist_ok=True)
        except Exception as exc:
            raise Exception("create class Logger error: {}".format(str(exc)))

    @property
    def folder_name(self):
        return self.__folder_name

    @property
    def file_name(self):
        return self.__file_name

    @property
    def error(self):
        raise RuntimeError('This property has no getter!')

    @property
    def success(self):
        raise RuntimeError('This property has no getter!')

    @property
    def warning(self):
        raise RuntimeError('This property has no getter!')

    def __logger(self, level: str, messages: Union[Iterable[str], str]) -> None:
        timestamp = datetime.now().isoformat()

        f = open(normpath("{}/{}".format(self.folder_name, self.file_name)), mode="a")

        if type(messages) is str:
            f.write("[{}] - {} - {}\n".format(timestamp, level, messages))
            f.write("\n")
        else:
            f.write("[{}] - {}:\n".format(timestamp, level))
            f.writelines(messages)
            f.write("\n")

        f.close()

    @error.setter
    def error(self, messages: Union[Iterable[str], str]):
        self.__logger(self.ERROR, messages)

    @success.setter
    def success(self, messages: Union[Iterable[str], str]):
        self.__logger(self.SUCCESS, messages)

    @warning.setter
    def warning(self, messages: Union[Iterable[str], str]):
        self.__logger(self.WARNING, messages)
