from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from selenium.webdriver.remote.webdriver import WebDriver


class BasePage:
    def __init__(self, driver: 'WebDriver'):
        self._driver = driver
