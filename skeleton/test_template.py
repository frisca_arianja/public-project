import sys

from typing import TYPE_CHECKING, Tuple, Callable
from re import match, findall
from selenium import webdriver
from selenium.webdriver.chrome.webdriver import Options
from webdriver_manager.chrome import ChromeDriverManager
from traceback import print_tb
from time import time

from .driver import Driver
from .lib.thread_timeout import thread_timeout

if TYPE_CHECKING:
    from selenium.webdriver.remote.webdriver import WebDriver


class TestTemplate():
    def __init__(self, url, driver: 'WebDriver' = None):
        if driver is None:
            chrome_opts = webdriver.ChromeOptions()
            chrome_opts.add_argument("--ignore-certificate-errors")

            chrome_caps = chrome_opts.to_capabilities()

            driver = webdriver.Chrome(ChromeDriverManager().install(),
                                      desired_capabilities=chrome_caps)
            driver.maximize_window()

            self._driver = Driver(driver)
        else:
            self._driver = driver

        self.__url = url

    def setup(self):
        self._driver.goto(self.__url)

    def teardown(self):
        self._driver.quit()

    def __set_timeout(self, timeout_opts: int, timeout: int) -> int:
        if timeout:
            return timeout
        elif timeout_opts:
            return timeout_opts

        return 10

    def __dummy_func(self):
        return

    def __set_setup_teardown(self,
                             caller_func: str) -> Tuple[Callable, Callable]:
        if not match(r"test_[Cc]\d+", caller_func):
            return (self.setup, self.teardown)

        return (self.__dummy_func, self.__dummy_func)

    @staticmethod
    def test_func(timeout: int = None):
        def wrapper(func):
            def func_decorator(self,
                               timeout_opts: int = None,
                               *args,
                               **kwargs):
                caller_func = sys._getframe().f_back.f_code.co_name
                setup, teardown = self.__set_setup_teardown(caller_func)

                timeout_value = self.__set_timeout(timeout_opts, timeout)

                start_time = 0

                try:
                    setup()

                    start_time = time()

                    ret = thread_timeout(timeout_value)(func)(self, *args,
                                                              **kwargs)
                except TimeoutError as err:
                    ret = (False,
                           TimeoutError(
                               "Timeout after running for {} s".format(
                                   timeout_value)).with_traceback(
                                       err.__traceback__))
                except Exception as exc:
                    ret = (False, exc)

                end_time = time() if start_time > 0 else 0
                teardown()

                return (*ret, self._driver.browser, self._driver.version,
                        (end_time - start_time))

            return func_decorator

        return wrapper
