import time
import json
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from skeleton.base_page import BasePage
from selenium.webdriver.common.by import By


class Category_Option(BasePage): 

    dropdown_btn_id = 'gh-shop-a'
    category_xpath = '//*[@id="gh-sbc"]/tbody/tr/td[1]/h3[2]/a'
    sub_category_xpath = '//*[@id="gh-sbc"]/tbody/tr/td[1]/ul[2]/li[4]/a'
    smartphone_category_nav_xpath = '//*[@id="s0-28-13_2-0-1[0]-0-0"]/ul/li[3]/a'
    more_refinements_btn_xpath = '//*[@id="s0-28-13-0-1[1]-0-6-2"]/button'
    screen_size_xpath = '//*[@id="c3-mainPanel-Screen%20Size"]/span'
    screen_size_opt1_xpath = '//*[@id="c3-subPanel-Screen%20Size_4.0%20-%204.4%20in_cbx"]'
    price_xpath = '//*[@id="c3-mainPanel-price"]/span'
    min_price_xpath = '//*[@id="c3-subPanel-_x-price-textrange"]/div/div[1]/div/input'
    max_price_xpath = '//*[@id="c3-subPanel-_x-price-textrange"]/div/div[2]/div/input'
    location_xpath = '//*[@id="c3-mainPanel-location"]/span'
    location_asia_xpath = '//*[@id="c3-subPanel-location_Asia"]/label/input'
    apply_btn_xpath = '//*[@id="c3-footerId"]/div[2]/button'

    screen_title = '//*[@id="s0-28-13-0-1[1]-0-6-0-0[0]"]/li[6]/div/h3'
    size_selected = '//*[@id="s0-28-13-0-1[1]-0-6-0-0[0]-multiselect[]_33"]/a'

    price_title = '//*[@id="s0-28-13-0-1[1]-0-6-0"]/li[2]/div/h3'
    min_price_input = '//*[@id="s0-28-13-0-1[1]-0-6-0-3[2]-textrange"]/div/div[1]/div/input'
    max_price_input = '//*[@id="s0-28-13-0-1[1]-0-6-0-3[2]-textrange"]/div/div[2]/div/input'

    loc_title = '//*[@id="s0-28-13-0-1[1]-0-6-0"]/li[4]/div/h3'
    loc_selected = '//*[@id="s0-28-13-0-1[1]-0-6-0-singleselect[]_9"]/a/span[1]'

    def click_dropdown_btn(self):
        self._driver.find_element(
            By.ID, Category_Option.dropdown_btn_id).is_displayed()
        title_text = self._driver.find_element(
            By.ID, Category_Option.dropdown_btn_id).text
        self._driver.find_element(
            By.ID, Category_Option.dropdown_btn_id).click()
        return title_text

    def category_title(self):
        self._driver.find_element(
            By.XPATH, Category_Option.category_xpath).is_displayed()
        category_title_text = self._driver.find_element(
            By.XPATH, Category_Option.category_xpath).text
        return category_title_text

    def subcategory_title(self):
        self._driver.find_element(
            By.XPATH, Category_Option.sub_category_xpath).is_displayed()
        sub_category_title_text = self._driver.find_element(
            By.XPATH, Category_Option.sub_category_xpath).text
        self._driver.find_element(
            By.XPATH, Category_Option.sub_category_xpath).click()
        return sub_category_title_text
    
    def choose_smartphone_category(self):
        self._driver.find_element(
            By.XPATH, Category_Option.smartphone_category_nav_xpath).is_displayed()
        category_title = self._driver.find_element(
            By.XPATH, Category_Option.smartphone_category_nav_xpath).text
        self._driver.find_element(
            By.XPATH, Category_Option.smartphone_category_nav_xpath).click()
        return category_title

    def click_more_refinements_btn(self):
        self._driver.find_element(
            By.XPATH, Category_Option.more_refinements_btn_xpath).is_displayed()
        self._driver.find_element(
            By.XPATH, Category_Option.more_refinements_btn_xpath).click()

    def choose_screen_size(self):
        self._driver.find_element(
            By.XPATH, Category_Option.screen_size_xpath).is_displayed()
        self._driver.find_element(
            By.XPATH, Category_Option.screen_size_xpath).click()
        time.sleep(3)
        self._driver.find_element(
            By.XPATH, Category_Option.screen_size_opt1_xpath).click()
    
    def choose_price(self):
        self._driver.find_element(
            By.XPATH, Category_Option.price_xpath).is_displayed()
        self._driver.find_element(
            By.XPATH, Category_Option.price_xpath).click()
        time.sleep(3)

    def input_min_price(self, min_price: str):
        self._driver.find_element(
            By.XPATH, Category_Option.min_price_xpath).send_keys(min_price)

    def input_max_price(self, max_price: str):
        self._driver.find_element(
            By.XPATH, Category_Option.max_price_xpath).send_keys(max_price)

    def choose_location(self):
        self._driver.find_element(
            By.XPATH, Category_Option.location_xpath).is_displayed()
        self._driver.find_element(
            By.XPATH, Category_Option.location_xpath).click()
        time.sleep(3)
        self._driver.find_element(
            By.XPATH, Category_Option.location_asia_xpath).click()
    
    def click_apply(self):
        self._driver.find_element(
            By.XPATH, Category_Option.apply_btn_xpath).is_displayed()
        self._driver.find_element(
            By.XPATH, Category_Option.apply_btn_xpath).click()

    def screen_size_result(self):
        self._driver.find_element(
            By.XPATH, Category_Option.screen_title).is_displayed()
        title = self._driver.find_element(
            By.XPATH, Category_Option.screen_title).text
        result = self._driver.find_element(
            By.XPATH, Category_Option.size_selected).text
        print(result)
        return title, result

    def price_result(self):
        self._driver.find_element(
            By.XPATH, Category_Option.price_title).is_displayed()
        title = self._driver.find_element(
            By.XPATH, Category_Option.price_title).text
        return title

    def check_price_value(self):
        min_price = self._driver.find_element(
            By.XPATH, Category_Option.min_price_input).get_attribute('value')
        max_price = self._driver.find_element(
            By.XPATH, Category_Option.max_price_input).get_attribute('value')
        return min_price, max_price

    def location_result(self):
        self._driver.find_element(
            By.XPATH, Category_Option.loc_title).is_displayed()
        title = self._driver.find_element(
            By.XPATH, Category_Option.loc_title).text
        result = self._driver.find_element(
            By.XPATH, Category_Option.loc_selected).text
        return title, result