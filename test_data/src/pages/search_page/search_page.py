import time
import json
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from skeleton.base_page import BasePage
from selenium.webdriver.common.by import By

class SearchPage(BasePage): 

    search_id = "gh-ac"
    dropdown_id = 'gh-cat-td'
    category_xpath = '//*[@id="gh-cat"]/option[35]'
    search_btn_xpath =  '//*[@id="gh-btn"]'
    first_result_xpath = '//*[@id="srp-river-results-listing1"]/div/div[2]/a/h3'

    def input_item_name (self, name: str):
        self._driver.find_element(
            By.ID, SearchPage.search_id).send_keys(name)

    def click_dropdown(self): 
        self._driver.find_element(
            By.ID, SearchPage.dropdown_id).is_displayed()
        self._driver.find_element(
            By.ID, SearchPage.dropdown_id).click()
  
    def choose_category(self): 
        self._driver.find_element(
            By.XPATH, SearchPage.category_xpath).is_displayed()
        self._driver.find_element(
            By.XPATH, SearchPage.category_xpath).click()

    def click_search_btn(self): 
        self._driver.find_element(
            By.XPATH, SearchPage.search_btn_xpath).is_displayed()
        self._driver.find_element(
            By.XPATH, SearchPage.search_btn_xpath).click()

    def check_result(self):
        title = self._driver.find_element(
            By.XPATH, SearchPage.first_result_xpath).text
        return title