import selenium.webdriver.support.expected_conditions as EC
import selenium.webdriver.support.ui as ui
from selenium.webdriver.common.by import By

from skeleton.base_page import BasePage


class AssertionElement(BasePage):

    def is_visible(self, locator, timeout=5):
        try:
            ui.WebDriverWait(self._driver, timeout).until(
                EC.visibility_of_element_located((By.XPATH, locator)))
            return True
        except BaseException as exc:
            return False, exc
