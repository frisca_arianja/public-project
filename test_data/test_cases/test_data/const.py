from os import getcwd

DOMAIN = "https://www.ebay.com/"
size_selected = "4.0 - 4.4 in"
min_price = "1,000,000"
max_price = "4,000,000"
location_selected = "Asia"
item_name = "PS4"
result_item = 'Sony PS4 Console 500GB + NEW Subsonic Controller Jet Black-Playstation 4'

prefix = "PT"
name = "Sejahtera"
suffix = "Tbk"
industry_id = "1"
employee_size = "500"
street = "Jl.Sudirman kav. 21"
place = "Sudirman Tower"
geograph_id = 100
phone = "08561290092"
URL = "https://www.mocky.io/"
