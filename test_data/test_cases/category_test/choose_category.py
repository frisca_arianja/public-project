
from test_data.src.pages.category_option_page.category_option import Category_Option
from test_data.test_cases.test_data import const
from test_data.test_cases.utils.assert_element import AssertionElement
from skeleton.test_template import TestTemplate
from selenium.webdriver.common.by import By
import time

class Choose_category(TestTemplate):

    def __init__(self, driver):
        super().__init__(const.DOMAIN, driver)

# choose subcategory 
    @TestTemplate.test_func()
    def test_C1654(self):
        try:
            category_option = Category_Option(self._driver)
            
            dropdown_title = category_option.click_dropdown_btn()
            assert dropdown_title == "Shop by category", "Shop by category title not match"

            category_title = category_option.category_title()
            assert category_title == "Electronics", "Category title selected not match"

            sub_category_title = category_option.subcategory_title()
            assert sub_category_title == "Cell phones & accessories", "Sub category title selected not match"

            nav_category_title = category_option.choose_smartphone_category()
            assert nav_category_title == "Cell Phones & Smartphones", "Nav category title selected not match"
            time.sleep(3)

            category_option.click_more_refinements_btn()
            time.sleep(3)
            category_option.choose_screen_size()
            time.sleep(2)
            category_option.choose_price()
            category_option.input_min_price(const.min_price)
            category_option.input_max_price(const.max_price)
            category_option.choose_location()
            category_option.click_apply()
            time.sleep(4)

            title,result = category_option.screen_size_result()
            assert title == "Screen Size", "Filter title not match"
            assert result == const.size_selected, "Option screen size selected not match"

            title = category_option.price_result()
            assert title == "Price", "Filter price title not match"

            min_price, max_price = category_option.check_price_value()
            assert min_price == const.min_price, "Minimum price not match"
            assert max_price == const.max_price, "Maximum price not match"

            title,result = category_option.location_result()
            assert title == "Item Location", "Filter title not match"
            assert result == const.location_selected, "Option location selected not match"

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc