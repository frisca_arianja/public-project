from test_data.src.pages.search_page.search_page import SearchPage
from test_data.test_cases.test_data import const
from test_data.test_cases.utils.assert_element import AssertionElement
from skeleton.test_template import TestTemplate
from selenium.webdriver.common.by import By
import time

class Search_Item(TestTemplate):

    def __init__(self, driver):
        super().__init__(const.DOMAIN, driver)

# choose category 
    @TestTemplate.test_func()
    def test_C1655(self):
        try:
            search_item = SearchPage(self._driver)

            search_item.input_item_name(const.item_name)
            search_item.click_dropdown()
            search_item.choose_category()
            search_item.click_search_btn()
            time.sleep(2)

            text =  search_item.check_result()
            assert text == const.result_item, "result is not match"

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc 