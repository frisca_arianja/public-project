
# Web Automation Test

This repository is contains Automation Testing of Public Project.


## Requirements

The requirements are

- [Python >3](https://www.python.org/downloads/)
- webdriver-manager >= 1.8.2
- selenium >= 3.141.0
- Faker >= 2.1.0
- environs >= 6.1.0

## Use Virtual Environment

We need to using pipenv in order to create a virtual environment whenever create a new python project. We use pipenv and we can install it by execute this command:

	# install pipenv
	pip install pipenv

We use python 3 for the virtual environment:

	# use python 3 for the virtual environment
	pipenv --python 3

Don't forget to change version of python to python 3 in Pipfile.

	[requires]
	python_version = "3"

Then that virtual environment by execute this command:

	# use previous virtual environment
	pipenv shell

## Copy environment variable

Install python-dotenv by execute this command:

	# install python-dotenv
	pipenv install python-dotenv

Copy the environment variable:

	# copy the environment variable
	cp env.sample .env


Input neccesary informations such as username and password to your .env file.
TR_PASSWORD is get from API Key at Test Rail My Settings menu.

## Install packages

Install requests by execute this command:

	# install requests
	pipenv install requests

Install Python's selenium package for the project by execute this command:

	# install selenium
	pipenv install selenium

## How to use

1.  Clone the selenium-testing-skeleton project

    `!IMPORTANT! Don't clone the selenium-testing-skeleton project in your test project`

2.  Copy all files .py and utils folder into your test project.

3.  If you have requirements.txt file, copy all items in requirements.txt on this project into your requirements.txt file. If you don't have requirements.txt file, copy the requirements.txt on this project into your test project

4.  Create run.json with format

    ```json
    {
    	"CXXX": "location/of/the/test/case/code.Class"
    	"unautomated": []
    }
    ```

    Unautomated is list of unautomated test cases

5.  If you have .gitignore file, copy all items in .gitignore-example on this project into your .gitignore file. If you don't have .gitignore file, copy the .gitignore-example on this project into your test project and rename it into .gitignore.

6. Change directory back to the project root then execute this command:

	# example how to execute
	python3 main.py -c 11485 -r 114

C11485 is the Test Case number.
![](C11485.png)

R114 is the Test Run number.
![](R114.png)