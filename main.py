import sys
from skeleton.runner import runner_main
from skeleton.config import access_env
from skeleton.lib.message_printing import error_print

if __name__ == "__main__":
    ret_env = access_env()
    if not ret_env[0]:
        error_print("Error access_env(): " + str(ret_env[1]), end="\n")

    ret = runner_main(sys.argv[1:])
    if not ret[0]:
        error_print("Error runner_main(): " + str(ret[1]), end="\n")
